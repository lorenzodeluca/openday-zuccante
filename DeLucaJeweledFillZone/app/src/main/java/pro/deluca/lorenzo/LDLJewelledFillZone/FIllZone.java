package pro.deluca.lorenzo.LDLJewelledFillZone;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

import static pro.deluca.lorenzo.LDLJewelledFillZone.Util.*;

public class FIllZone extends AppCompatActivity {
    Context context=this;
    static public int colorN=6;
    GridLayout colorSelector;
    TextView pointZone;
    ImageView[] colors=new ImageView[colorN];
    GridLayout grid;
    static long point=0;
    static ImageView[][] cell = new ImageView[rowCount][columnCount];
    static int cellStatus[][] = new int[100][100];
    static int cellStatusBackup[][] = new int[100][100];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fillzone);
        grid=findViewById(R.id.gameGridFill);
        pointZone=findViewById(R.id.pointZoneFillZ);
        colorSelector=findViewById(R.id.colorSelectorFillZ);
        point=0;
        loadCells();
        initCell();
        loadColorSelector();
        initColorSelector();
        gridUpdate();
    }
    void loadCells() {
        grid.removeAllViews();
        grid.setRowCount(rowCount);
        grid.setColumnCount(columnCount);
        int lato = screenWidth(getWindowManager());
        cell=new ImageView[rowCount+1][columnCount+1];
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                cell[r][c] = new ImageView(this);
                cell[r][c].setMaxWidth(lato / (rowCount-1));
                grid.addView(cell[r][c], lato / (columnCount), lato / (columnCount));
            }
    }
    void pointUpdate(int toAdd){
        point+=toAdd;
        pointZone.setText(getString(R.string.scoreFillZtext)+" "+point);
    }
    void loadColorSelector(){
        colorSelector.removeAllViews();
        colorSelector.setRowCount(1);
        colorSelector.setColumnCount(colorN);
        int lato = screenWidth(getWindowManager());
        for (int c = 0; c < colorN; c++) {
            colors[c] = new ImageView(this);
            colors[c] .setMaxWidth(lato / (colorN));
            colorSelector.addView(colors[c], lato / (colorN), 200);
        }
    }
    void initCell() {
        Random gen = new Random();
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                cellStatus[r][c] = gen.nextInt(6) + 1;
            }
    }
    boolean finished(){
        boolean ris=true;
        for (int r = 0; r < rowCount&&ris; r++)
            for (int c = 0; c < columnCount-1&&ris; c++) {
                if(cellStatus[r][c]!=cellStatus[r][c+1])ris=false;
            }
        return ris;
    }
    void initColorSelector() {
        colors[0].setImageResource(R.color.cell1);
        colors[1].setImageResource(R.color.cell2);
        colors[2].setImageResource(R.color.cell3);
        colors[3].setImageResource(R.color.cell4);
        colors[4].setImageResource(R.color.cell5);
        colors[5].setImageResource(R.color.cell6);
        for(int i=0;i<colorN;i++){
            final int finalI = i;
            colors[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        filler(finalI);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    void filler(int color) throws InterruptedException {
        //Log.v("onClickEvent",""+color);
        pointUpdate(1);
        fill(cellStatus[0][0],color+1,0,0);// (0,0) = top-left
        gridUpdate();
        if(finished()){
            new AlertDialog.Builder(this)
                    .setTitle("You win!!!!")
                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(context, Menu.class));
                        }
                    })
                    .setMessage(getString(R.string.scoreFillZoneBeforeValue)+" "+point+" "+getString(R.string.afterPointsScoreTextFillZ))
                    .setNeutralButton("Play again", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(context, FIllZone.class));
                        }})
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
    //recursive function
    void fill(int colorToReplace,int replaceWith,int x,int y) throws InterruptedException {
        if(cellStatus[x][y]==colorToReplace){
            cellStatus[x][y]=replaceWith;
            if(x<rowCount-1)fill(colorToReplace,replaceWith,x+1,y);
            if(y<columnCount-1)fill(colorToReplace,replaceWith,x,y+1);
        }
        else return;
    }
    void gridUpdate() {
        //final ArrayList<Animator> toAnimate=new ArrayList<>();
        cell[0][0].setImageResource(R.drawable.secchiello);
        //aggiorno i colori
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                switch (cellStatus[r][c]) {
                    case 0: //clear
                        cell[r][c].setBackgroundResource(R.drawable.clear);
                        //cell[r][c].setImageResource(R.drawable.clear);
                        break;
                    case 1: //case A
                        cell[r][c].setBackgroundResource(R.color.cell1);
                        //cell[r][c].setImageResource(R.color.cell1);
                        break;
                    case 2: //case A
                        cell[r][c].setBackgroundResource(R.color.cell2);
                        //cell[r][c].setImageResource(R.color.cell2);
                        break;
                    case 3: //case A
                        cell[r][c].setBackgroundResource(R.color.cell3);
                        //cell[r][c].setImageResource(R.color.cell3);
                        break;
                    case 4: //case A
                        cell[r][c].setBackgroundResource(R.color.cell4);
                        //cell[r][c].setImageResource(R.color.cell4);
                        break;
                    case 5: //case A
                        cell[r][c].setBackgroundResource(R.color.cell5);
                        //cell[r][c].setImageResource(R.color.cell5);
                        break;
                    case 6: //case A
                        cell[r][c].setBackgroundResource(R.color.cell6);
                        //cell[r][c].setImageResource(R.color.cell6);
                        break;
                }
                if (cellStatus[r][c] != cellStatusBackup[r][c]) {
                    cellStatusBackup[r][c] = cellStatus[r][c];
                    final ObjectAnimator anim = ObjectAnimator.ofFloat(cell[r][c], "RotationY", 0, 180);
                    //toAnimate.add(anim);
                    final AnimatorSet a = new AnimatorSet();
                    a.setDuration(200);
                    a.playSequentially(anim);
                    a.start();
                    a.addListener(new AnimatorListenerAdapter() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            AnimatorSet ar = new AnimatorSet();
                            ar.setDuration(0);
                            ar.playSequentially(anim);
                            ar.start();
                            ar.reverse();
                        }
                    });
                }
            }

    }

}
