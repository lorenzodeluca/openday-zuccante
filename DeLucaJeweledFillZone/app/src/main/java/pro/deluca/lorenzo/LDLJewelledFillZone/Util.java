package pro.deluca.lorenzo.LDLJewelledFillZone;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class Util {
    static public int columnCount = 5;
    static public int rowCount = 3;
    public static int screenWidth = 0;
    public static int screenHeight = 0;

    static int toPositive(int num) {
        if (num < 0) return num * -1;
        else return num;
    }

    static int toNegative(int num) {
        if (num > 0) return num * -1;
        else return num;
    }

    public static int screenWidth(WindowManager win) {
        if (screenWidth == 0) {
            Display display = win.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            final int width = size.x;
            screenWidth = width;
        }
        return screenWidth;
    }
    public static int screenHeight(WindowManager win) {
        if (screenHeight == 0) {
            Display display = win.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            final int height = size.y;
            screenHeight = height;
        }
        return screenHeight;
    }
}
