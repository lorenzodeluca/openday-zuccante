package pro.deluca.lorenzo.LDLJewelledFillZone;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

import static pro.deluca.lorenzo.LDLJewelledFillZone.Util.columnCount;
import static pro.deluca.lorenzo.LDLJewelledFillZone.Util.rowCount;

public class Jeweled extends AppCompatActivity {
    Context context = this;
    static public int cellStatusBackup[][] = new int[100][100];
    static ImageView[][] cell = new ImageView[rowCount][columnCount];
    static int cellStatus[][] = new int[100][100];
    GridLayout grid;
    static long point;
    static TextView pointZone;
    static int sinceLastSpecial = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeweled);
        grid = (GridLayout) findViewById(R.id.gameGridCandy);
        grid.setBackgroundResource(R.drawable.background);
        pointZone = findViewById(R.id.textZoneCandy);
        //carica imageview cell
        loadCells();
        //setGrid();
        initCell();
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++)
                cellStatusBackup[r][c] = cellStatus[r][c];
        while (cellsToDelete()) {
            gameUpdate();
        }
        gridUpdate(true);
        point = 0;
        pointZone.setText("Score: " + point);
        checkIfMovesAvaible();
        //imposto OnClickListeners
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                final int r2 = r, c2 = c;
                final int finalR = r;
                final int finalC = c;
                final int finalR1 = r;
                final int finalC1 = c;
                cell[r][c].setOnTouchListener(new View.OnTouchListener() {
                    float x, y;
                    boolean replaced = false;

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            x = event.getX();
                            y = event.getY();
                            return true;
                        }
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            boolean swipe = false;
                            //decomment to accept only the swipes of max 2 cell lenght
                            /*if((event.getX()>x+(cell[0][0].getHeight()*2))||(event.getY()>y+(cell[0][0].getHeight()*2)))return false;
                            else */
                            if (event.getX() > x + cell[0][0].getWidth()) {
                                Log.v("onTouch", "swipe dx");
                                swipe = true;
                                //sostituzione con cella a destra
                                try {
                                    if (finalC < columnCount - 1)
                                        replaced = replaceTwoCell(finalR, finalC, finalR1, finalC1 + 1);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else if (event.getX() < x - cell[0][0].getWidth()) {
                                Log.v("onTouch", "swipe sx");
                                swipe = true;
                                //sostituzione con cella a sinistra
                                try {
                                    if (finalC > 0)
                                        replaced = replaceTwoCell(finalR1, finalC1 - 1, finalR, finalC);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else if (event.getY() > y + cell[0][0].getHeight()) {
                                Log.v("onTouch", "swipe down");
                                swipe = true;
                                //sostituzione con cella sotto
                                try {
                                    if (finalR < rowCount - 1)
                                        replaced = replaceTwoCell(finalR1, finalC1, finalR + 1, finalC);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else if (event.getY() < y - cell[0][0].getHeight()) {
                                Log.v("onTouch", "swipe w/upper");
                                swipe = true;
                                //sostituzione con cella sopra
                                try {
                                    if (finalR > 0)
                                        replaced = replaceTwoCell(finalR1 - 1, finalC1, finalR, finalC);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else if (event.getX() < x + cell[0][0].getHeight() && event.getX() >= x) {//single click
                                Log.v("onTouch", "clickkkk");
                                if (cellStatus[r2][c2] < 10)//pulsante attualmente non schiacciato
                                    cellStatus[r2][c2] += 10;
                                else cellStatus[r2][c2] -= 10;
                                rotationAnimation(r2,c2);
                                gridUpdate(true);
                                //aggiorno tab
                                if (selectedCount() > 1) {
                                    Log.v("ontouch", "replacing");
                                    try {
                                        replaced = replaceTwoCell();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    gameUpdate();
                                    deselectAll();
                                    if (voidCellCount() > 0) {
                                        //gestione celle vuote
                                        fixSpaces();
                                    }
                                    while (cellsToDelete()) {
                                        gameUpdate();
                                    }
                                    gridUpdate(!replaced);
                                }
                            }
                            if (swipe) {
                                gameUpdate();
                                if (voidCellCount() > 0) replaced = true;
                                if (voidCellCount() > 0) {
                                    //gestione celle vuote
                                    fixSpaces();
                                }
                                gridUpdate(!replaced);
                                if (cellsToDelete()) replaced = true;
                                while (cellsToDelete()) {
                                    gameUpdate();
                                }
                                gridUpdate(!replaced);
                            }
                            checkIfMovesAvaible();
                            return true;
                        }
                        return false;
                    }
                });
            }
    }

    //Viene chiamato all'inizio per sistemare le combinazioni già generate nella mappa di gioco random
    static void fixGameAllAuto() {
        //while(cellsToDelete())gameUpdate();
        if (voidCellCount() > 0) {
            //gestione celle vuote
            fixSpaces();
        }
        //gridUpdate();
    }

    static void pointUpdate(int toAdd) {
        point += toAdd;
        pointZone.setText("Score: " + point);
    }

    static void fixSpaces() {
        while (voidCellCount() > 0) {
            Random gen = new Random();
            boolean specialAlreadySetted = false;
            for (int r = 0; r < rowCount; r++)
                for (int c = 0; c < columnCount; c++) {
                    //aggiungo nuovi celle alla prima riga
                    if (r == 0) {
                        if (cellStatus[r][c] == 0) {
                            if (point > 200) {//with special cell
                                if (gen.nextInt(100) > 30) {
                                    if (sinceLastSpecial > (rowCount) && !specialAlreadySetted) {
                                        cellStatus[r][c] = gen.nextInt(6) + 1;
                                        sinceLastSpecial = 0;
                                        specialAlreadySetted = true;
                                    } else if (!specialAlreadySetted) {
                                        sinceLastSpecial++;
                                        specialAlreadySetted = true;
                                    }
                                } else {
                                    cellStatus[r][c] = gen.nextInt(5) + 1;
                                }
                            } else {
                                cellStatus[r][c] = gen.nextInt(5) + 1;
                            }
                        }
                    } else {
                        //faccio scendere le celle che hanno uno spazio sotto
                        if (cellStatus[r - 1][c] != 0 && cellStatus[r][c] == 0) {
                            cellStatus[r][c] = cellStatus[r - 1][c];
                            cellStatus[r - 1][c] = 0;
                        }
                    }
                }
        }
    }

    static int countCellWithValue(int value) {
        int count = 0;
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                if (cellStatus[r][c] == value) count++;
            }
        return count;
    }

    static boolean cellsToDelete() {
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount - 2; c++)
                if (toBasic(cellStatus[r][c]) == toBasic(cellStatus[r][c + 1]) && toBasic(cellStatus[r][c + 1]) == toBasic(cellStatus[r][c + 2]))
                    return true;
        for (int r = 0; r < rowCount - 2; r++)
            for (int c = 0; c < columnCount; c++)
                if (toBasic(cellStatus[r][c]) == toBasic(cellStatus[r + 1][c]) && toBasic(cellStatus[r + 1][c]) == toBasic(cellStatus[r + 2][c]))
                    return true;
        return false;
    }

    boolean cellsToDelete(int[][] cel) {
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount - 2; c++)
                if (toBasic(cel[r][c]) == toBasic(cel[r][c + 1]) && toBasic(cel[r][c + 1]) == toBasic(cel[r][c + 2]))
                    return true;
        for (int r = 0; r < rowCount - 2; r++)
            for (int c = 0; c < columnCount; c++)
                if (toBasic(cel[r][c]) == toBasic(cel[r + 1][c]) && toBasic(cel[r + 1][c]) == toBasic(cel[r + 2][c]))
                    return true;
        Log.v("cellsToDelete", "false");
        return false;
    }

    int selectedCount() {
        int counter = 0;
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++)
                if (cellStatus[r][c] > 10) counter++;
        return counter;
    }

    boolean finished() {
        boolean ris = true;
        for (int r = 0; r < rowCount && ris; r++)
            for (int c = 0; c < columnCount && ris; c++) {
                if ((c < columnCount - 1) && pointAvaibleNearby(r, c, r, c + 1, cellStatus[r][c + 1])) {
                    ris = false;
                } else if ((r < rowCount - 1) && pointAvaibleNearby(r, c, r + 1, c, cellStatus[r + 1][c])) {
                    ris = false;
                } else if (countCellWithValue(6) > 0) ris = false;
            }
        return ris;
    }

    void checkIfMovesAvaible() {
        if (finished()) {
            new AlertDialog.Builder(context)
                    .setTitle("Game finished")
                    .setMessage(getString(R.string.beforeScoreCandy) + " " + point)
                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(context, Menu.class));
                        }
                    })
                    .setNeutralButton("Play again", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(context, Jeweled.class));
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    static int voidCellCount() {
        int counter = 0;
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++)
                if (cellStatus[r][c] == 0) counter++;
        return counter;
    }

    boolean replaceTwoCell() throws InterruptedException {
        int cell1RC[] = new int[2];
        cell1RC[0] = -99999;// 0=row 1=columm
        int cell2RC[] = new int[2];
        cell2RC[0] = -99998;
        for (int r = 0; r < rowCount && cell2RC[0] == -99998; r++)
            for (int c = 0; c < columnCount; c++) {
                if (cellStatus[r][c] > 10) {
                    if (cell1RC[0] == -99999) {
                        cell1RC[0] = r;
                        cell1RC[1] = c;
                    } else {
                        cell2RC[0] = r;
                        cell2RC[1] = c;
                    }
                }
            }
        return replaceTwoCell(cell1RC[0], cell1RC[1], cell2RC[0], cell2RC[1]);
    }

    boolean replaceTwoCell(int x1, int y1, int x2, int y2) throws InterruptedException {
        int cell1RC[] = new int[2];
        cell1RC[0] = x1;// 0=row 1=columm
        cell1RC[1] = y1;
        int cell2RC[] = new int[2];
        cell2RC[0] = x2;
        cell2RC[1] = y2;
        //checking if the 2 cells are nearby
        deselectAll();
        if (cellStatus[cell1RC[0]][cell1RC[1]] == 6 || cellStatus[cell2RC[0]][cell2RC[1]] == 6) {//special value
            int[] cellsToDelete;
            int[] specialCell;
            if (cellStatus[cell1RC[0]][cell1RC[1]] == 6) {
                cellsToDelete = cell2RC;//same pointers
                specialCell = cell1RC;
            } else {
                cellsToDelete = cell1RC;
                specialCell = cell2RC;
            }
            int valueToDelete = cellStatus[cellsToDelete[0]][cellsToDelete[1]];
            for (int r = 0; r < rowCount; r++) {
                for (int c = 0; c < columnCount; c++) {
                    if (toBasic(cellStatus[r][c]) == toBasic(valueToDelete)) cellStatus[r][c] = 0;
                }
            }
            //remove of the cell with the powers
            cellStatus[specialCell[0]][specialCell[1]] = 0;
        }
        //if((cell1RC[0]==cell2RC[0]+1||cell1RC[0]==cell2RC[0]-1) || (cell1RC[1]==cell2RC[1]+1||cell1RC[1]==cell2RC[1]-1)){
        else if ((cell1RC[0] == cell2RC[0] + 1 && cell1RC[1] == cell2RC[1]) || (cell1RC[0] == cell2RC[0] - 1 && cell1RC[1] == cell2RC[1]) || (cell1RC[1] == cell2RC[1] + 1 && cell1RC[0] == cell2RC[0]) || (cell1RC[1] == cell2RC[1] - 1 && cell1RC[0] == cell2RC[0])) {
            if (pointAvaibleNearby(cell2RC[0], cell2RC[1], cell1RC[0], cell1RC[1], cellStatus[cell1RC[0]][cell1RC[1]]) || pointAvaibleNearby(cell1RC[0], cell1RC[1], cell2RC[0], cell2RC[1], cellStatus[cell2RC[0]][cell2RC[1]])) {
                //swap
                Semaphore waitFlag = new Semaphore(0);
                if (cell1RC[0] == cell2RC[0]) {
                    cellReplaceWithAnimazion(cell1RC[0], cell1RC[1], cell2RC[0], cell2RC[1], cell[0][0].getHeight(), 0, 200, waitFlag);
                } else {
                    cellReplaceWithAnimazion(cell1RC[0], cell1RC[1], cell2RC[0], cell2RC[1], 0, cell[0][0].getHeight(), 200, waitFlag);
                }
                waitFlag.acquireUninterruptibly();
                int tmp = cellStatus[cell1RC[0]][cell1RC[1]];
                cellStatus[cell1RC[0]][cell1RC[1]] = cellStatus[cell2RC[0]][cell2RC[1]];
                cellStatus[cell2RC[0]][cell2RC[1]] = tmp;
                Log.v("Replacer", "acquired");
                return true;
            } else {
                if (cell1RC[0] == cell2RC[0]) {
                    cellAnimatorWithReturn(cell1RC[0], cell1RC[1], cell2RC[0], cell2RC[1], cell[0][0].getHeight(), 0, 200, 200);
                } else {
                    cellAnimatorWithReturn(cell1RC[0], cell1RC[1], cell2RC[0], cell2RC[1], 0, cell[0][0].getHeight(), 200, 200);
                }
                return false;
            }
        } else {
            //annullo selezioni
            deselectAll();
            return false;
        }
        return true;
    }

    void cellAnimatorWithReturn(final int x1, final int y1, final int x2, final int y2, final int xmove, final int ymove, final int time, final int timeReturn) throws InterruptedException {
        deselectAll();
        Log.v("AnimWithReturn", "start");
        ObjectAnimator movX1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationX", 0, xmove);
        ObjectAnimator movY1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationY", 0, ymove);
        ObjectAnimator movX2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationX", 0, -xmove);
        ObjectAnimator movY2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationY", 0, -ymove);
        final AnimatorSet set = new AnimatorSet();
        set.setDuration(time);
        set.playTogether(movX1, movY1, movX2, movY2);
        set.start();
        set.addListener(new AnimatorListenerAdapter() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ObjectAnimator mov2X1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationX", 0, xmove);
                ObjectAnimator mov2Y1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationY", 0, ymove);
                ObjectAnimator mov2X2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationX", 0, -xmove);
                ObjectAnimator mov2Y2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationY", 0, -ymove);
                AnimatorSet set2 = new AnimatorSet();
                set2.setDuration(timeReturn);
                set2.playTogether(mov2X1, mov2X2, mov2Y2, mov2Y1);
                set2.start();
                set2.reverse();//viene subito fatta l'animazione inversa
            }
        });
        Log.v("AnimWithReturn", "stopped");
    }

    void cellReplaceWithAnimazion(final int x1, final int y1, final int x2, final int y2, final int xmove, final int ymove, final int time, final Semaphore finished) {
        deselectAll();
        ObjectAnimator movX1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationX", 0, xmove);
        ObjectAnimator movY1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationY", 0, ymove);
        ObjectAnimator movX2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationX", 0, -xmove);
        ObjectAnimator movY2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationY", 0, -ymove);
        final AnimatorSet set = new AnimatorSet();
        set.setDuration(time);
        set.playTogether(movX1, movY1, movX2, movY2);
        set.start();
        set.addListener(new AnimatorListenerAdapter() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ObjectAnimator mov2X1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationX", 0, xmove);
                ObjectAnimator mov2Y1 = ObjectAnimator.ofFloat(cell[x1][y1], "TranslationY", 0, ymove);
                ObjectAnimator mov2X2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationX", 0, -xmove);
                ObjectAnimator mov2Y2 = ObjectAnimator.ofFloat(cell[x2][y2], "TranslationY", 0, -ymove);
                AnimatorSet set2 = new AnimatorSet();
                set2.setDuration(0);
                set2.playTogether(mov2X1, mov2X2, mov2Y2, mov2Y1);
                set2.start();
                set2.reverse();//viene subito fatta l'animazione inversa
            }
        });
        finished.release();
    }

    boolean pointAvaibleNearby(int cellR1, int cellC1, int cellR2, int cellC2, int value) {
        fixGameAllAuto();
        int cellTemp[][] = new int[rowCount][columnCount];
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                cellTemp[r][c] = cellStatus[r][c];
            }
        cellTemp[cellR2][cellC2] = cellTemp[cellR1][cellC1];
        cellTemp[cellR1][cellC1] = value;
        if (!cellsToDelete(cellTemp)) return false;
        return true;
    }

    void initCell() {
        Random gen = new Random();
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                cellStatus[r][c] = gen.nextInt(5) + 1;
            }
        gridUpdate(true);
    }

    static void gameUpdate() {
        //deselect all
        deselectAll();
        //horizontal check
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount - 2; c++) {
                if (toBasic(cellStatus[r][c]) == toBasic(cellStatus[r][c + 1]) && toBasic(cellStatus[r][c + 1]) == toBasic(cellStatus[r][c + 2]) && toBasic(cellStatus[r][c]) != 0) {
                    int daElim = cellStatus[r][c];
                    for (int del = c; del < columnCount && cellStatus[r][del] == daElim; del++) {
                        cellStatus[r][del] = Util.toNegative(cellStatus[r][del]);//i mark the cell as "must be deleted"
                        pointUpdate(20);
                    }
                }
            }
        //vertical check
        for (int r = 0; r < rowCount - 2; r++)
            for (int c = 0; c < columnCount; c++) {
                if (toBasic(cellStatus[r][c]) == toBasic(cellStatus[r + 1][c]) && toBasic(cellStatus[r + 1][c]) == toBasic(cellStatus[r + 2][c]) && cellStatus[r][c] != 0) {
                    int daElim = toBasic(cellStatus[r][c]);
                    for (int del = r; del < rowCount && toBasic(cellStatus[del][c]) == daElim; del++) {
                        cellStatus[del][c] = Util.toNegative(cellStatus[del][c]);//i mark the cell as "must be deleted"
                        pointUpdate(20);
                    }
                }
            }
        //sequences deletion loop
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                if (cellStatus[r][c] < 0) {
                    cellStatus[r][c] = 0;
                }
            }
        fixSpaces();
    }

    static void deselectAll() {
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                cellStatus[r][c] = Util.toPositive(cellStatus[r][c]);
                if (cellStatus[r][c] > 10) {
                    cellStatus[r][c] -= 10;
                    Log.v("replace2cell", "r2c" + cellStatus[r][c]);
                }
            }
    }

    static int toBasic(int num) {
        if (num > 10) return Util.toPositive(num - 10);
        else return Util.toPositive(num);
    }

    static int countChangesSinceLastBackup() {
        int counter = 0;
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++)
                if (cellStatus[r][c] != cellStatusBackup[r][c]) counter++;
        Log.v("changesSinceLastBackup", "" + counter);
        return counter;
    }

    static int countNegative() {
        int counter = 0;
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++)
                if (cellStatus[r][c] < 0) counter++;
        Log.v("countNegative", "" + counter);
        return counter;
    }

    void rotationAnimation(int r,int c) {
        final ObjectAnimator anim= ObjectAnimator.ofFloat(cell[r][c], "RotationY", 0, 180);
        final AnimatorSet a=new AnimatorSet();
        a.setDuration(200);
        a.playTogether(anim);
        a.start();
        a.addListener(new AnimatorListenerAdapter() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                AnimatorSet ar=new AnimatorSet();
                ar.setDuration(0);
                ar.playTogether(anim);
                ar.start();
                ar.reverse();
            }
        });
    }

    static void gridUpdate(boolean justSelection) {
        Log.v("gridUpdate","justselection"+justSelection);
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                if(cellStatus[r][c]!=cellStatusBackup[r][c]){
                    if(justSelection)//cellSelected
                    {
                        cellStatusBackup[r][c]=cellStatus[r][c];
                        final ObjectAnimator anim= ObjectAnimator.ofFloat(cell[r][c], "RotationY", 0, 180);
                        final AnimatorSet a=new AnimatorSet();
                        a.setDuration(200);
                        a.playTogether(anim);
                         //a.start();
                        a.addListener(new AnimatorListenerAdapter() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                AnimatorSet ar=new AnimatorSet();
                                ar.setDuration(0);
                                ar.playTogether(anim);
                                ar.start();
                                ar.reverse();
                            }
                        });
                    }
                    else if(r==0) {//new cell added
                        cellStatusBackup[r][c]=cellStatus[r][c];
                        final ObjectAnimator anim1= ObjectAnimator.ofFloat(cell[r][c], "ScaleX", 0, (float)1);
                        final ObjectAnimator anim2= ObjectAnimator.ofFloat(cell[r][c], "ScaleY", 0, (float)1);
                        final AnimatorSet a=new AnimatorSet();
                        a.setDuration(400);
                        a.playTogether(anim1,anim2);
                        a.start();
                        a.addListener(new AnimatorListenerAdapter() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                AnimatorSet ar=new AnimatorSet();
                                ar.setDuration(0);
                                ar.playTogether(anim1,anim2);
                                //ar.start();
                                //ar.reverse();
                            }
                        });
                    }
                    else if(r!=rowCount-1){
                        cellStatusBackup[r][c]=cellStatus[r][c];
                        final ObjectAnimator anim= ObjectAnimator.ofFloat(cell[r][c], "translationY", 0, cell[2][2].getHeight());
                        final AnimatorSet a=new AnimatorSet();
                        a.setDuration(200);
                        a.playTogether(anim);
                        a.start();
                        final int finalR = r;
                        final int finalC = c;
                        a.addListener(new AnimatorListenerAdapter() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                ObjectAnimator animr= ObjectAnimator.ofFloat(cell[finalR][finalC], "translationY", 0, (float)cell[2][2].getHeight()/2);
                                AnimatorSet ar=new AnimatorSet();
                                ar.setDuration(100);
                                ar.playTogether(animr);
                                ar.start();
                                ar.reverse();
                                ar.addListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        cellStatusBackup[finalR][finalC]=cellStatus[finalR][finalR];
                                        final ObjectAnimator anim= ObjectAnimator.ofFloat(cell[finalR][finalC], "translationY", 0, (float)cell[2][2].getHeight()/4);
                                        final AnimatorSet a=new AnimatorSet();
                                        a.setDuration(75);
                                        a.playTogether(anim);
                                        a.start();
                                        a.addListener(new AnimatorListenerAdapter() {
                                            @RequiresApi(api = Build.VERSION_CODES.O)
                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                super.onAnimationEnd(animation);
                                                ObjectAnimator animr= ObjectAnimator.ofFloat(cell[finalR][finalC], "translationY", 0, (float)cell[2][2].getHeight()/6);
                                                AnimatorSet ar=new AnimatorSet();
                                                ar.setDuration(50);
                                                ar.playTogether(animr);
                                                ar.start();
                                                ar.reverse();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    else {
                        cellStatusBackup[r][c]=cellStatus[r][c];
                        final ObjectAnimator anim= ObjectAnimator.ofFloat(cell[r][c], "RotationY", 0, 180);
                        final AnimatorSet a=new AnimatorSet();
                        a.setDuration(200);
                        //a.playTogether(anim);
                        //a.start();
                        a.addListener(new AnimatorListenerAdapter() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                AnimatorSet ar=new AnimatorSet();
                                ar.setDuration(0);
                                ar.playTogether(anim);
                                //ar.start();
                                //ar.reverse();
                            }
                        });
                    }
                }
            }

        //aggiorno i colori
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                switch (cellStatus[r][c]) {
                    case 0: //clear
                        //cell[r][c].setBackgroundResource(R.drawable.clear);
                        cell[r][c].setImageResource(R.drawable.clear);
                        break;
                    case 1: //case A
                        //cell[r][c].setBackgroundResource(R.drawable.a);
                        cell[r][c].setImageResource(R.drawable.a);
                        break;
                    case 11: //case A selected
                        //cell[r][c].setBackgroundResource(R.drawable.aselected);
                        cell[r][c].setImageResource(R.drawable.aselected);
                        break;
                    case 2: //case B
                        //cell[r][c].setBackgroundResource(R.drawable.b);
                        cell[r][c].setImageResource(R.drawable.b);
                        break;
                    case 12://case B selected
                        //cell[r][c].setBackgroundResource(R.drawable.bselected);
                        cell[r][c].setImageResource(R.drawable.bselected);
                        break;
                    case 3://case C
                        //cell[r][c].setBackgroundResource(R.drawable.c);
                        cell[r][c].setImageResource(R.drawable.c);
                        break;
                    case 13://case C selected
                        //cell[r][c].setBackgroundResource(R.drawable.cselected);
                        cell[r][c].setImageResource(R.drawable.cselected);
                        break;
                    case 4: //case D
                        //cell[r][c].setBackgroundResource(R.drawable.d);
                        cell[r][c].setImageResource(R.drawable.d);
                        break;
                    case 14: //case D selected
                        //cell[r][c].setBackgroundResource(R.drawable.dselected);
                        cell[r][c].setImageResource(R.drawable.dselected);
                        break;
                    case 5: //case E
                        //cell[r][c].setBackgroundResource(R.drawable.e);
                        cell[r][c].setImageResource(R.drawable.e);
                        break;
                    case 15: //case E selected
                        //cell[r][c].setBackgroundResource(R.drawable.eselected);
                        cell[r][c].setImageResource(R.drawable.eselected);
                        break;
                    case 6: //case Virus
                        //cell[r][c].setBackgroundResource(R.drawable.e);
                        cell[r][c].setImageResource(R.drawable.virus);
                        break;
                    case 16: //case Virus selected
                        //cell[r][c].setBackgroundResource(R.drawable.e);
                        cell[r][c].setImageResource(R.drawable.virusselected);
                        break;
                }
            }
    }

    void loadCells() {
        grid.removeAllViews();
        grid.setRowCount(rowCount);
        grid.setColumnCount(columnCount);
        int lato = Util.screenWidth(getWindowManager());
        cell=new ImageView[rowCount+1][columnCount+1];
        for (int r = 0; r < rowCount; r++)
            for (int c = 0; c < columnCount; c++) {
                cell[r][c] = new ImageView(this);
                cell[r][c].setMaxWidth(lato / (rowCount - 1));
                grid.addView(cell[r][c], lato / (columnCount), lato / (columnCount));
            }
    }
}
