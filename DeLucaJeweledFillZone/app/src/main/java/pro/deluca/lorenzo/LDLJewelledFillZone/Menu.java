package pro.deluca.lorenzo.LDLJewelledFillZone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import static pro.deluca.lorenzo.LDLJewelledFillZone.Util.columnCount;
import static pro.deluca.lorenzo.LDLJewelledFillZone.Util.rowCount;
import static pro.deluca.lorenzo.LDLJewelledFillZone.Util.screenWidth;

public class Menu extends AppCompatActivity {
    Context con=this;
    ImageView backg;
    SeekBar bar;
    TextView diffFillZ;
    TextView diffCandyC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Button menuCandy= findViewById(R.id.candyCrush);
        Button menuFill=findViewById(R.id.fillZone);
        backg=findViewById(R.id.backgroundMenu);
        bar=findViewById(R.id.difficultBar);
        diffCandyC=findViewById(R.id.difficultTextCandy);
        diffFillZ=findViewById(R.id.difficultTextFillZ);
        fixDimensions();
        menuCandy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(con, Jeweled.class));
            }
        });
        menuFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(con, FIllZone.class));
            }
        });
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //progress 1-100,
                //progress:100=rowCount:20
                rowCount=((progress*19)/100)+3;//value 4-24
                if(rowCount<8)columnCount=5;
                else columnCount=5+(rowCount/2);
                if(rowCount>20)columnCount++;
                if(rowCount<9){
                    diffCandyC.setText(R.string.hard);
                    diffFillZ.setText(R.string.easy);
                }
                else if(rowCount<15){
                    diffCandyC.setText(R.string.medium);
                    diffFillZ.setText(R.string.medium);
                }
                else if(rowCount<21){
                    diffCandyC.setText(R.string.easy);
                    diffFillZ.setText(R.string.hard);
                }
                Log.v("ProgressBar","Row="+rowCount+"; column="+columnCount);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
    void fixDimensions(){
        backg.setX(0);
        backg.setY(0);
        backg.setMinimumWidth(screenWidth(getWindowManager())+100);
        backg.setMaxWidth(screenWidth(getWindowManager())+100);
    }

}
