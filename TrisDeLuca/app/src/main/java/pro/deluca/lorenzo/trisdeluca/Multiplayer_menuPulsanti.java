package pro.deluca.lorenzo.trisdeluca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.content.Intent;

public class Multiplayer_menuPulsanti extends AppCompatActivity {
    Button singleplay2p, doubleplay2p, high2p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player_selector);
        singleplay2p = (Button) findViewById(R.id.singleplay2p);
        doubleplay2p = (Button) findViewById(R.id.doubleplay2p);
        high2p = (Button) findViewById(R.id.high2P);
        singleplay2p.setOnClickListener(
                v -> {
                    Intent intent = new Intent(Multiplayer_menuPulsanti.this, Multiplayer_names.class);
                    startActivity(intent);
                }
        );
        doubleplay2p.setOnClickListener(
                v -> {
                    Intent intent = new Intent(Multiplayer_menuPulsanti.this, TwoDevice2P_names.class);
                    startActivity(intent);
                }
        );
        high2p.setOnClickListener(
                v -> {
                    Intent intent = new Intent(Multiplayer_menuPulsanti.this, Highscores2P.class);
                    startActivity(intent);
                }
        );
    }
}
