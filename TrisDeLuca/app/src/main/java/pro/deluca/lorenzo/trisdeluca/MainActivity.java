package pro.deluca.lorenzo.trisdeluca;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import static pro.deluca.lorenzo.trisdeluca.R.id.RGtwoPlayer;

public class MainActivity extends AppCompatActivity {

    Button b1, b2, b3, play, highScore;
    boolean singleSelected = true;
    RadioButton singleDevice;
    RadioGroup  twoPlayer;
    SharedPreferences sharedPrefs;
    TextView AI_textName, AI_textValue, USER_textName, USER_textValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPrefs = getSharedPreferences("NAMES", MODE_PRIVATE);
        Multiplayer_names.p1Name = sharedPrefs.getString("PLAYER1", Multiplayer_names.p1Name);
        Multiplayer_names.p2Name = sharedPrefs.getString("PLAYER2", Multiplayer_names.p2Name);
        TwoDevice2P_names.MyName = sharedPrefs.getString("MYNAME", TwoDevice2P_names.MyName);
        AI_textName=(TextView)findViewById(R.id.aiScore);
        AI_textValue=(TextView)findViewById(R.id.aiScoreResult);
        USER_textName=(TextView)findViewById(R.id.playerScore);
        USER_textValue=(TextView)findViewById(R.id.playerScoreResult);
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        b3 = (Button) findViewById(R.id.b3);
        play = (Button) findViewById(R.id.play);
        highScore = (Button) findViewById(R.id.highScores);
        highScore.setVisibility(View.INVISIBLE);
        singleDevice = (RadioButton) findViewById(R.id.singleRBtn);
        singleDevice.setChecked(true);
        twoPlayer = (RadioGroup) findViewById(RGtwoPlayer);
        twoPlayer.setVisibility(View.INVISIBLE);
        b2.setAlpha((float) 0.3);
        b1.setOnClickListener(
                v -> {
                    b1.setAlpha((float) 1);
                    b2.setAlpha((float) 0.3);
                    singleSelected = true;
                    twoPlayer.setVisibility(View.INVISIBLE);
                    highScore.setVisibility(View.INVISIBLE);
                    AI_textName.setVisibility(View.VISIBLE);
                    AI_textValue.setVisibility(View.VISIBLE);
                    USER_textName.setVisibility(View.VISIBLE);
                    USER_textValue.setVisibility(View.VISIBLE);
                }
        );
        b2.setOnClickListener(
                v -> {
                    b1.setAlpha((float) 0.3);
                    b2.setAlpha((float) 1);
                    singleSelected = false;
                    twoPlayer.setVisibility(View.VISIBLE);
                    highScore.setVisibility(View.VISIBLE);
                    AI_textName.setVisibility(View.INVISIBLE);
                    AI_textValue.setVisibility(View.INVISIBLE);
                    USER_textName.setVisibility(View.INVISIBLE);
                    USER_textValue.setVisibility(View.INVISIBLE);
                }
        );
        b3.setOnClickListener(
                v -> {
                    Intent intent;
                    intent = new Intent(MainActivity.this, About.class);
                    startActivity(intent);
                }
        );
        play.setOnClickListener(
                v -> {
                    if (singleSelected) {
                         Intent intent;
                        intent = new Intent(MainActivity.this, Singleplayer.class);
                        startActivity(intent);

                    } else {
                        if (singleDevice.isChecked()) {
                            Intent intent;
                            intent = new Intent(MainActivity.this, Multiplayer_names.class);
                            startActivity(intent);
                        } else {
                            Intent intent;
                            intent = new Intent(MainActivity.this, TwoDevice2P_names.class);
                            startActivity(intent);
                        }
                    }
                }
        );

        highScore.setOnClickListener(
                v -> {
                    Intent intent = new Intent(MainActivity.this, Highscores2P.class);
                    startActivity(intent);
                }
        );
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //updating the Singleplayer RigaPunteggioPerDb
        AI_textValue.setText(Integer.toString(Highscores2P.scoreAi));
        USER_textValue.setText(Integer.toString(Highscores2P.scorePlayer));
    }
    @Override
    public void onStop() {
        super.onStop();

        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString("PLAYER1", Multiplayer_names.p1Name);
        edit.putString("PLAYER2", Multiplayer_names.p2Name);
        edit.putString("MYNAME", TwoDevice2P_names.MyName);
        edit.commit();
    }
}
