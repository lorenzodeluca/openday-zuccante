package pro.deluca.lorenzo.trisdeluca;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Singleplayer extends AppCompatActivity {

    public static Activity act_1d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        act_1d = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player);
    }
}
