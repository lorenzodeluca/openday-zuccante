package pro.deluca.lorenzo.trisdeluca;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class Highscores2P extends AppCompatActivity {

    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscores2p);
        Button clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseHandler dbh = new DatabaseHandler(Highscores2P.this);
                        dbh.deleteAll();
                        finish();
                    }
                }
        );
        TextView list1 = (TextView) findViewById(R.id.list1);
        TextView list2 = (TextView) findViewById(R.id.list2);
        DatabaseHandler dbh = new DatabaseHandler(this);
        List<RigaPunteggioPerDb> res = dbh.getAllContacts();
        for (RigaPunteggioPerDb sb : res) {
            i++;
            list1.setText(list1.getText() + "\n" + i + ". " + sb.get_name());
            list2.setText(list2.getText() + "\n" + sb.get_score());
        }
    }
    public static int scoreAi=0,scorePlayer=0;
}
