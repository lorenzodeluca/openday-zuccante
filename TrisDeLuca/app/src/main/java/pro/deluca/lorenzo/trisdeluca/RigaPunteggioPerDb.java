package pro.deluca.lorenzo.trisdeluca;

/**
 * @author Lorenzo De Luca
 * @date 19/02/2018
 * Riga di dati per il database
 */
public class RigaPunteggioPerDb {
    private String _name;
    private int _score;
    private int _id;

    public RigaPunteggioPerDb() {

    }

    public RigaPunteggioPerDb(int id, String name, int score) {
        _id = id;
        _name = name;
        _score = score;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public int get_score() {
        return _score;
    }

    public void set_score(int _score) {
        this._score = _score;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
    public static int ai_score=0;
    public static int user_score=0;
}
